from __future__ import absolute_import

from rest_framework.exceptions import ValidationError

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST


def error_response(message=None, status=HTTP_400_BAD_REQUEST, extra_data=None):
    response_content = {
        'status': 'error',
    }

    if message is not None:
        response_content['message'] = message

    extra_data = extra_data or {}
    response_content.update(extra_data)
    return Response(response_content, status=status)


def success_response(message=None, status=HTTP_200_OK, extra_data=None):
    response_content = {
        'status': 'ok',
    }

    if message is not None:
        response_content['message'] = message

    extra_data = extra_data or {}
    response_content.update(extra_data)
    return Response(response_content, status=status)


class CustomResponse(object):
    """Mixin for unifying response structure"""
    def _compose_error(self, error):
        error_item = error.detail.serializer.errors.items()[-1]
        error_field_name = error_item[0]
        error_text = error_item[1][0]
        delimiter = ":"
        if error_field_name == 'non_field_errors':
            error_field_name = delimiter = ''
        error_text = "{name}{delimiter} {error}".format(name=error_field_name, delimiter=delimiter, error=error_text)
        return error_text

    def response(self, method, request, *args, **kwargs):
        """django REST framework raises exception if there are validation errors, so I used this hook
        to redirect request-response flow"""
        unbound_method = getattr(super(CustomResponse, self), method, None)
        if not unbound_method:
            handler = self.http_method_not_allowed
            response = handler(request, *args, **kwargs)
            return response
        try:
            res = unbound_method(request, *args, **kwargs)
            res = success_response(res.data, status=res.status_code)
        except ValidationError as err:
            res = error_response(self._compose_error(err), status=err.status_code)
        return res

    def get(self, request, *args, **kwargs):
        return self.response('get', request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.response('post', request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.response('put', request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.response('patch', request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.response('delete', request, *args, **kwargs)

# Usage example
# class LoopDetail(CustomResponse, RetrieveAPIView, CreateAPIView, UpdateAPIView):
#     queryset = Loop.objects.all()
#     permission_classes = [IsAuthenticated]
#
#     def get_serializer(self, *args, **kwargs):
#         serializer = LoopCreateUpdateSerializer(*args, **kwargs)
#         if self.request.method == 'GET':
#             upgraded = self.request.user.upgraded()
#             serializer = LoopSerializer(*args, context={'upgraded': upgraded}, **kwargs)
#         return serializer

