Hook that allows chain queries.
Standard way is just use:

```
# models.py
class SomeModelQuerySet(models.QuerySet):
    def very_reusable_query():
        return self.filter(**some_bunch_of_lookups)


class SomeModelManager(models.Manager)
    def get_queryset(self):
        return SomeModelQuerySet(self.model, using=self._db)


class SomeModel(models.Model)
    field1 = models.SomeField()
    field2 = models.SomeField()
    field3 = models.SomeField()
    
    objects = SomeModelManager.from_queryset(SomeModelQuerySet)()
```

```
# example_query.py

...
example = SomeModel.objects.very_reusable_query()
...
```

This example allows you to use chained filtering with properties, this can't be
reached with above standard way. 

```
...
example = SomeModel.objects.active.filter(date__range=some_range)
...
```


Reason: In respect to django source code:
 
```
# django/db/models/manager.py
...
class BaseManager(...):
    ...
    @classmethod
    def _get_queryset_methods(cls, queryset_class):
        def create_method(name, method):
            def manager_method(self, *args, **kwargs):
                return getattr(self.get_queryset(), name)(*args, **kwargs)
            manager_method.__name__ = method.__name__
            manager_method.__doc__ = method.__doc__
            return manager_method

        new_methods = {}
        # Refs http://bugs.python.org/issue1785.
        predicate = inspect.isfunction if six.PY3 else inspect.ismethod  # THIS LINE (they should fix it with additional
                                                                         # lookup for 'inspect.isdatadescriptor' to
                                                                         # allow properties 
        for name, method in inspect.getmembers(queryset_class, predicate=predicate):
            # Only copy missing methods.
            if hasattr(cls, name):
                continue
            # Only copy public methods or methods with the attribute `queryset_only=False`.
            queryset_only = getattr(method, 'queryset_only', None)
            if queryset_only or (queryset_only is None and name.startswith('_')):
                continue
            # Copy the method onto the manager.
            new_methods[name] = create_method(name, method)
        return new_methods
    ...
```