from django.db import models
from django.db.models import Q


class Delegate(object):
    """This hook allows to chain queryset filtering with properties.
    Use SomeManager.from_queryset(SomeQuerySet)() if you want to chain method as function calls"""

    def __getattr__(self, attr, *args):
        try:
            return getattr(self.__class__, attr, *args)
        except AttributeError:
            return getattr(self.get_queryset(), attr, *args)


class FeedItemQuerySet(models.QuerySet):
    @property
    def self_written(self):
        twitter_post = Q(twitterpost__isnull=False)
        is_retweet = Q(twitterpost__is_retweet=True)
        is_child_post = ~Q(twitterpost__parent_post_twitter_id__exact='')
        is_quoted_retweet = ~Q(twitterpost__quoted_status_id_str__exact='')

        instagram_posts = Q(instagrampost__isnull=False)
        twitter_posts = twitter_post & (~(is_retweet | is_child_post) | is_quoted_retweet)

        q = instagram_posts | twitter_posts
        return self.filter(q)


class FeedItemManager(Delegate, models.Manager):
    def get_queryset(self):
        return FeedItemQuerySet(self.model, using=self._db)


class FeedItem(models.Model):
    sportsman = models.ForeignKey("SomeModel", related_name='feed', null=True, blank=True)
    created_at = models.DateTimeField()
    deleted = models.NullBooleanField(default=False)

    objects = FeedItemManager()
