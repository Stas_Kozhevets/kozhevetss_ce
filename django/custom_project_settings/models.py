from django.core.exceptions import ValidationError
from django.db import models


class CustomSetting(models.Model):
    """Provide one setting of any type accessed in common way as '.value'
    All settings field names must end with '_value'"""
    name = models.CharField(max_length=30, unique=True)
    description = models.TextField()

    string_value = models.CharField(max_length=200, null=True, blank=True)
    boolean_value = models.NullBooleanField(default=None, null=True, blank=True)
    integer_value = models.IntegerField(null=True, blank=True)
    decimal_value = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    image_value = models.ImageField(null=True, blank=True)

    def __unicode__(self):
        return '<Setting : {name} : {_type} : {value}>'.format(name=self.name, _type=type(self.value), value=self.value)

    def clean(self):
        # empty string value can't be used
        if not self.string_value:
            self.string_value = None

        # at this point only one field should contain non-None value
        if len(self.value_check_mass) != 1:
            raise ValidationError('Choose one setting')

    @property
    def value(self):
        mass = self.value_check_mass
        return mass[0] if mass else None

    @property
    def value_check_mass(self):
        return filter(lambda x: x is not None, self.values_fields())

    @classmethod
    def _values_fields(cls):
        fields = cls._meta.get_all_field_names()
        value_fields = [field for field in fields if field.endswith('_value')]
        return value_fields

    def values_fields(self):
        return [getattr(self, name) for name in self.__class__._values_fields()]
