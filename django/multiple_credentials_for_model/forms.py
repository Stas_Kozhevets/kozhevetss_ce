from django import forms
from django.core.exceptions import ValidationError

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit


class CredentialsForm(forms.Form):
    login = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'download-input'}))
    password = forms.CharField(max_length=30, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.credentials_fk_field = kwargs.pop(getattr(self, 'credentials_fk_field'))
        super(CredentialsForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        layout = self.helper.layout = Layout()

        layout.append(Field('login', placeholder='Username'))
        layout.append(Field('password', placeholder='Password'))

        btn_name = getattr(self, 'btn_name')

        if callable(btn_name):
            btn_name = btn_name()

        self.helper.add_input(Submit('submit', btn_name, css_class="btn btn-lg btn-primary btn-block"))
        self.helper.form_show_labels = False
        self.helper.form_class = 'download-control-form'

    def clean(self):
        login = self.cleaned_data.get('login', '').lower()
        password = self.cleaned_data.get('password', '').lower()

        # standard:
        approved = self.credentials_fk_field.credentials.filter(login=login, password=password).exists()
        if not approved:
            raise ValidationError("Wrong access details")

        # ignore case version
        combinations = map(lambda x: (x.login.lower(), x.password.lower()), self.credentials_fk_field.credentials.all())
        if not any([True for combination in combinations if combination == (login, password)]):
            raise ValidationError("Wrong access details")


class DownloadControlForm(CredentialsForm):
    credentials_fk_field = 'very_long_and_weird_name'

    # if btn name should be static - just provide string
    btn_name = lambda self: 'Get %s' % self.credentials_fk_field.name
