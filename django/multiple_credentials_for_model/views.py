from models import DownloadPage
from forms import DownloadControlForm

def download_page_view(request, name):
    lookup = {'name': name}
    download_page = DownloadPage.objects.filter(**lookup)

    # ...

    if request.method == 'POST':
        form = DownloadControlForm(request.POST, very_long_and_wierd_name=download_page)
        # ...
    else:
        form = DownloadControlForm(very_long_and_weird_name=download_page)

    # ...