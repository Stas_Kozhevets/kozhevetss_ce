from django.db import models

from mixins import CredentialsMixin


class DownloadPage(models.Model):
    name = models.CharField(max_length=250, unique=True)

    @property
    def has_credentials(self):
        return True if self.credentials.exists() else False


class Protected(CredentialsMixin, models.Model):
    download_page = models.ForeignKey(DownloadPage, related_name='credentials')

    class Meta:
        unique_together = ('download_page', 'login', 'password',)
