from django.db import models


class CredentialsMixin(models.Model):
    login = models.CharField(max_length=30)
    password = models.CharField(max_length=30)

    class Meta:
        abstract = True

    def __unicode__(self):
        return "{}: {}".format(self.login, self.password)
