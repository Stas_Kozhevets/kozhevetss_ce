from lxml import html
from operator import attrgetter
from inspect import getmembers, isclass, ismethod
from importlib import import_module
import requests

from utilize.api.twitter import twitter
from utilize.utils import to_time_2


class NoneIndex(list):
    def __getitem__(self, item):
        try:
            return super(NoneIndex, self).__getitem__(item)
        except IndexError:
            return None


class Article(object):
    __slots__ = ['title', 'body', 'timestamp', 'resource']

    def __init__(self, title, body, timestamp):
        self.title = title
        self.body = body
        self.timestamp = timestamp


class ProtoReactor(object):
    """Base class for crawlers"""
    def __len__(self):
        return max(map(len, [self.titles, self.bodies, self.timestamps]))

    def titles(self):
        raise NotImplemented

    def bodies(self):
        raise NotImplemented

    def timestamps(self):
        raise NotImplemented

    @property
    def articles(self):
        """While importing crawler classes several methods will be wrapped with
        property decorator"""
        get_args = lambda i: (self.titles[i], self.bodies[i], self.timestamps[i])
        feed = [Article(*get_args(i)) for i in xrange(len(self))]
        return sorted(feed, key=attrgetter('timestamp'), reverse=True)


class ArticleXPATHReactor(ProtoReactor):
    def __init__(self, response):
        # Should be standard requests response
        self._response = response
        self._text = self._response.text
        self.xpath = html.fromstring(self._text).xpath
        self._length = 0

    @classmethod
    def crawl(cls):
        return requests.get(cls.resource)


class ArticleTwitterReactor(ProtoReactor):
    def __init__(self, response):
        self.response = response

    @classmethod
    def crawl(cls):
        user_id_str = twitter.get_user_by_username(cls.resource)['id_str']
        return twitter.load_user_feed(user_id_str)

    def titles(self):
        return [r['id_str'] for r in self.response]

    def bodies(self):
        return [r['text'] for r in self.response]

    def timestamps(self):
        return [to_time_2(r['created_at']) for r in self.response]


def all_properties(cls):
    import inspect

    predefined_methods = ['titles', 'bodies', 'timestamps', 'articles']
    methods = inspect.getmembers(cls, predicate=inspect.ismethod)
    for method in methods:
        if method[0] in predefined_methods:
            setattr(cls, method[0], property(method[1]))
    return cls


def _none_index(func):
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        if isinstance(res, NoneIndex):
            return res
        return NoneIndex(res)

    return wrapper


def none_index(cls):
    predefined_methods = ['titles', 'bodies', 'timestamps', 'articles']
    methods = getmembers(cls, predicate=ismethod)
    for method in methods:
        if method[0] in predefined_methods:
            setattr(cls, method[0], _none_index(method[1]))
    return cls


def import_crawlers(*args):
    crawlers_modules = map(import_module, args)
    produced = []
    for module in crawlers_modules:
        for cls in getmembers(module, predicate=isclass):
            if ProtoReactor in cls[1].__bases__[0].__bases__:
                non_indexed = none_index(cls[1])
                proper = all_properties(non_indexed)
                produced.append(proper)
    return produced
