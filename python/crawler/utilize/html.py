# coding: utf-8
import re
from functools import partial, update_wrapper

QUOTE_PATTERN = r"""([&<>"'])(?!(amp|lt|gt|quot|#39);)"""
PARTIAL_URL_PATTERN = r'(s?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)'
SOCIAL_PATTERN = r'(?<=^|(?<=[^a-zA-Z0-9-_\\.]))([@#]|http)([A-Za-z0-9][\w_.]+|' + PARTIAL_URL_PATTERN + ')'

QUOTE_COMPILED = re.compile(QUOTE_PATTERN)
SOCIAL_COMPILED = re.compile(SOCIAL_PATTERN)


def escape(word):
    """Replaces special characters <>&"' to HTML-safe sequences.
    With attention to already escaped characters."""
    replace_with = {
        '<': '&gt;',
        '>': '&lt;',
        '&': '&amp;',
        '"': '&quot;',  # should be escaped in attributes
    }

    return re.sub(QUOTE_COMPILED, lambda x: replace_with[x.group(0)], word).replace('#39', "'")


def html_text(social, text):
    """Process social posts (Twitter, Instagram) links, hashtags and etc.
    See partial implementation further."""

    def substitute(matchobj):
        instagram = {
            '@': "<a href='https://instagram.com/{1}'>@{1}</a>",
            '#': "<a href='https://instagram.com/explore/tags/{1}'>#{1}</a>",
        }

        twitter = {
            '@': "<a href='https://twitter.com/{1}'>@{1}</a>",
            '#': "<a href='https://twitter.com/hashtag/{1}?src=hash'>#{1}</a>",
        }

        result = {
            'twitter': twitter,
            'instagram': instagram
        }[social]

        result['http'] = "<a href='{0}{1}'>{0}{1}</a>"
        return result[matchobj.group(1)].format(matchobj.group(1), matchobj.group(2))

    escaped = escape(text)
    processed = re.sub(SOCIAL_COMPILED, substitute, escaped)
    return processed

# alter implementation for twitter post converting can be
# found as Twython.html_for_tweet staticmethod in twython library
twitter_html = partial(html_text, 'twitter')
instagram_html = partial(html_text, 'instagram')

# handle function introspection
update_wrapper(twitter_html, html_text)
update_wrapper(instagram_html, html_text)
