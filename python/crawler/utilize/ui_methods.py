from functools import partial
from operator import is_not, is_

is_not_none = partial(is_not, None)
is_none = partial(is_, None)


def trim(content, length=140, suffix='...'):
    if len(content) <= length:
        return content
    else:
        return ' '.join(content[:length + 1].split(' ')[0:-len(suffix)]) + suffix



