"""
Twython wrapper
-------
Decreasing redundant calls for access_token obtaining
Auto switching credentials pairs if rate limit exceeded. (Not implemented fully)
Rewritten twython cursor for statuses.
"""

import twython
from functools import wraps
from itertools import cycle

__all__ = ['twitter']

TWITTER_CREDENTIALS_PAIRS = [
    # (TWITTER_CLIENT_ID, TWITTER_CLIENT_SECRET)
    ('bdti4z6Ixrm3IyA3OIV1QPFcT', 'YDj011otXonq3yowWizAKcfM0kVTnIKvOEYq0kPtdu2pHPDCut'),
]


def handle_error_codes(error_return_val=None, codes=None, callback=None):
    """Generic exception handler. Not implemented.
    From twython internals:
        # greater than 304 (not modified) is an error

        # Twitter API 1.1, always return 429 when
        # rate limit is exceeded (Same as TwythonRateLimitError)

        # Twitter API 1.1, returns a 401 Unauthorized or
        # a 400 "Bad Authentication data" for invalid/expired
        # app keys/user tokens (Same as TwythonAuthError)"""
    codes = codes or []

    def wrap(func):
        @wraps(func)
        def decorator(*args, **kwargs):
            try:
                res = func(*args, **kwargs)
            except twython.TwythonError as error:
                if error.error_code not in codes:
                    raise
                res = error_return_val
            return res

        return decorator

    return wrap


class _twitter(object):
    def __init__(self, t_credentials):
        self._token = None
        self.cred_len = len(t_credentials)
        self._credentials = cycle(t_credentials)
        self.access = self._switch_credentials()

    def _switch_credentials(self, err=None):
        """In case of reaching Twitter RateLimit we can infinitely cycling
         over credentials pairs. Functionality not yet implemented"""
        if err and self.cred_len == 1:
            raise err
        new_cred = self._credentials.next()
        return self._get_access(*new_cred, enforce=True)

    def _get_access(self, api_key, api_secret, enforce=False):
        """Obtaining access token once"""
        if not self._token or enforce:
            twitter_auth = twython.Twython(api_key, api_secret, oauth_version=2)
            self._token = twitter_auth.obtain_access_token()
        access = twython.Twython(api_key, access_token=self._token)
        return access

    def get_user_by_username(self, username):
        try:
            users = self.access.lookup_user(screen_name=username.lower())
        except twython.TwythonError:
            return None
        users = filter(lambda user: user['screen_name'].lower() == username.lower(), users)
        if not users:
            return None
        return users[0]

    def get_users_by_id(self, *args):
        ids = ','.join(args)
        try:
            users = self.access.lookup_user(user_id=ids)
        except twython.TwythonError:
            return None
        return users

    def get_user_page(self, username):
        return 'https://twitter.com/%s' % username

    def load_user_feed(self, user_id, **kwargs):
        """Returns results with an ID greater than (that is, more recent than) the specified ID.
        There are limits to the number of Tweets which can be accessed through the API.
        If the limit of Tweets has occurred since the since_id, the since_id will be forced
        to the oldest ID available."""
        try:
            feed = self.access.get_user_timeline(user_id=user_id, **kwargs)
        except twython.TwythonError as err:
            feed = []
            if not str(err).startswith('HTTPSConnectionPool'):
                raise
        return feed

    def search_user_mentions(self, username, hashtags=tuple(), **kwargs):
        keywords = ['@%s' % username]
        keywords.extend(map(lambda hashtag: '#%s' % hashtag, hashtags))
        q = ' OR '.join(keywords)

        count = kwargs.get('count', None)
        if count:
            limited = True
        else:
            limited = False
            kwargs['count'] = 100

        result = {}
        init = True
        while init or 'next_results' in result['search_metadata']:
            if not init:
                kwargs['max_id'] = result['statuses'][-1]['id'] - 1
            try:
                result = self.access.search(q=q, **kwargs)
            except twython.TwythonError as err:
                if not str(err).startswith('HTTPSConnectionPool'):
                    raise
                break

            init = False
            for comment in result['statuses']:
                yield comment

            if limited:
                return

    def search_cursor(self, **kwargs):
        """Since native twython cursor may iterate infinitely without returning any results
        Usage:
            for item in search_cursor(kwargs):
                ...
        """
        init = True
        kwargs['count'] = 100
        result = {}
        while init or 'next_results' in result['search_metadata']:
            if not init:
                kwargs['max_id'] = result['statuses'][-1]['id'] - 1
            result = self.access.search(**kwargs)
            init = False
            yield result['statuses']


twitter = _twitter(TWITTER_CREDENTIALS_PAIRS)
