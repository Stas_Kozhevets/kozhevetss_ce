import random
import dateutil.parser

import time
import datetime


def r_string(ex_str):
    words = ex_str.split()
    random.shuffle(words)
    r_str = ' '.join([random.choice(words) for i in xrange(random.randint(5, 100))])
    return r_str


def to_time_1(string):
    raw = string.lower().strip().replace(',', '')
    weekday, str_month, day, year = raw.split()
    months = {
        'january': '1',
        'february': '2',
        'march': '3',
        'april': '4',
        'may': '5',
        'june': '6',
        'july': '7',
        'august': '8',
        'september': '9',
        'october': '10',
        'november': '11',
        'december': '12',
    }[str_month]
    date = datetime.date(int(year), int(months), int(day))
    return date


def to_time_2(string):
    return dateutil.parser.parse(string)


def to_js_timestamp(date_time, squash_date=False):
    if squash_date:
        date_time = date_time.replace(year=2010, month=1, day=1)
    return time.mktime(date_time.timetuple()) * 1000.0


def to_minute(date_time):
    dt_tuple = date_time.timetuple()
    return dt_tuple.tm_hour * 60 + dt_tuple.tm_min


def to_time(date_time):
    return date_time.strftime("%H:%M")


def random_html_color():
    random.seed(time.clock())
    return "#%06x" % random.randint(0, 0xFFFFFF)

def to_week_int_weekday(date_time):
    return date_time.isocalendar()[2]