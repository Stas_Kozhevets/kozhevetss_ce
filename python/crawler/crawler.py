import threading
import Queue

from meta import import_crawlers

test_crawlers = import_crawlers('crawlers')
# Django-like import magic. Performing all wrapping operations on crawler classes.

in_queue = Queue.Queue()
out_queue = Queue.Queue()
result_queue = Queue.Queue()


class RequestThread(threading.Thread):
    def __init__(self, in_queue, out_queue):
        super(RequestThread, self).__init__()
        self.in_queue = in_queue
        self.out_queue = out_queue

    def run(self):
        # Using 1 instead of True since python < 3.0 while loops optimized
        # for this true condition.
        while 1:
            crawler = self.in_queue.get()
            crawler.response = crawler.crawl()
            self.out_queue.put(crawler)
            self.in_queue.task_done()


class ParserThread(threading.Thread):
    def __init__(self, queue):
        super(ParserThread, self).__init__()
        self.queue = queue

    def run(self):
        while 1:
            crawler = self.queue.get()
            data_obj = crawler(crawler.response)
            for article in data_obj.articles:
                article.resource = crawler.resource
                result_queue.put(article)
            self.queue.task_done()


def crawler():
    """Producer of Article items."""
    for crawler in test_crawlers:
        t = RequestThread(in_queue, out_queue)
        t.setDaemon(True)
        t.start()
    for crawler in test_crawlers:
        in_queue.put(crawler)

    for crawler in test_crawlers:
        pt = ParserThread(out_queue)
        pt.setDaemon(True)
        pt.start()

    in_queue.join()
    out_queue.join()
    for item in xrange(result_queue.qsize()):
        yield result_queue.get()
