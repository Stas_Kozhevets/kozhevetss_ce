from meta import ArticleXPATHReactor, ArticleTwitterReactor
from utilize.utils import to_time_1


class BlogPythonOrgNews(ArticleXPATHReactor):
    resource = 'http://blog.python.org/'

    def titles(self):
        titles = self.xpath('//div[@class="date-outer"]//h3[@class="post-title entry-title"]/a/text()')
        return titles

    def bodies(self):
        bodies = self.xpath('//div[@class="date-outer"]//div[@class="post-body entry-content"]')
        bodies = [item.text_content().encode('utf-8').strip().decode('ascii', 'ignore') for item in bodies]
        return bodies

    def timestamps(self):
        timestamps = self.xpath('//div[@class="date-outer"]/h2[@class="date-header"]/span/text()')
        timestamps = [to_time_1(item) for item in timestamps]
        return timestamps


class PythonAgentNews(ArticleTwitterReactor):
    resource = 'Python_Agent'


class DigitalOceanNews(ArticleTwitterReactor):
    resource = 'digitalocean'
