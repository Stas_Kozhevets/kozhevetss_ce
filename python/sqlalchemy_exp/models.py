from __future__ import absolute_import
from urlparse import urlparse
import psycopg2

from sqlalchemy import create_engine, Text, ForeignKey, Boolean, DateTime, Table, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Sequence, CheckConstraint
from sqlalchemy.orm import relationship, sessionmaker


from .managers import ToggleNoteManager

# fh = logging.FileHandler(os.path.join(settings.BASE_DIR, 'logs','sqlalchemy.engine.log'))
# fh.level = logging.DEBUG
# logger = logging.getLogger('sqlalchemy.engine').addHandler(fh)


DB_SETTINGS = dict(
    database='newscrawler',
    user='XXXXX',
    password='XXXXX',
    host='XXXXX',
    port='5432'
)

def connect_pg():
    return psycopg2.connect(**DB_SETTINGS)


engine = create_engine('postgres://', creator=connect_pg, echo=False, connect_args={'client_encoding': 'utf8'})

Base = declarative_base()
metadata = MetaData(naming_convention={"ck": "ck_%(table_name)s_%(constraint_name)s"})


# ----------------------------------------------------------------------------------------------------------------------
#                                                NEWS MODELS
# ----------------------------------------------------------------------------------------------------------------------


class NewsSource(Base):
    __tablename__ = 'newscrawler_source'

    id = Column(Integer, Sequence('newscrawler_source_id_seq'), primary_key=True)
    url = Column(String)

    @property
    def resource(self):
        r = urlparse(self.url).netloc
        if not r:
            r = self.url
        return r


class NewsArticle(Base):
    """Primary table for saving articles. Machinery should be implemented on it as on raw data."""
    __tablename__ = 'newscrawler_article'

    id = Column(Integer, Sequence('newscrawler_article_id_seq'), primary_key=True)
    title = Column(String, nullable=False)
    body = Column(Text, nullable=False)
    article_timestamp = Column(String)
    timestamp = Column(String)
    rate = Column(Integer)
    hidden = Column(Boolean, default=False)

    source_id = Column(Integer, ForeignKey('newscrawler_source.id'))
    source = relationship(NewsSource, primaryjoin=source_id == NewsSource.id, backref='articles')

    CheckConstraint("title != ''", name='title_not_empty')
    CheckConstraint("body != ''", name='body_not_empty')


# ----------------------------------------------------------------------------------------------------------------------
#                                                TOGGLE MODELS
# ----------------------------------------------------------------------------------------------------------------------


class ToggleClient(Base):
    __tablename__ = 'toggle_client'

    id = Column(Integer, Sequence('toggle_client_id_seq'), primary_key=True)
    name = Column(String(200), nullable=False)

    CheckConstraint("name != ''", name='name_not_empty')

    def __repr__(self):
        return '<ToggleClient: {name}>'.format(name=self.name)


class ToggleProject(Base):
    __tablename__ = 'toggle_project'

    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    color = Column(String(7), nullable=False)

    client_id = Column(Integer, ForeignKey('toggle_client.id'))
    client = relationship(ToggleClient, primaryjoin=client_id == ToggleClient.id, backref='projects')

    CheckConstraint("name != ''", name='name_not_empty')
    CheckConstraint("color != ''", name='color_not_empty')

    def __repr__(self):
        return '<ToggleProject: {name}>'.format(name=self.name)


tag_note_linkage = Table(
    'tag_note_linkage', Base.metadata,
    Column('tag_id', Integer, ForeignKey('toggle_tag.id')),
    Column('note_id', Integer, ForeignKey('toggle_note.id')),
)


class ToggleNote(Base, ToggleNoteManager):
    __tablename__ = 'toggle_note'

    id = Column(Integer, primary_key=True)  # using toggle id for it
    start = Column(DateTime, nullable=False)
    end = Column(DateTime, nullable=False)
    duration = Column(Integer, nullable=False)  # it should be in seconds
    description = Column(String)

    project_id = Column(Integer, ForeignKey('toggle_project.id'))
    project = relationship(ToggleProject, primaryjoin=project_id == ToggleProject.id, backref='notes')

    tags = relationship("ToggleTags", secondary=tag_note_linkage, backref="notes")

    def __repr__(self):
        return '<ToggleNote: {start}>'.format(start=self.start.date())


class ToggleTags(Base):
    __tablename__ = 'toggle_tag'
    id = Column(Integer, Sequence('toggle_tag_id_seq'), primary_key=True)
    name = Column(String(30), nullable=False, unique=True)

    CheckConstraint("name != ''", name='name_not_empty')

    def __repr__(self):
        return '<ToggleTag: {name}>'.format(name=self.name)

# ----------------------------------------------------------------------------------------------------------------------
#                                                TEST MODELS
# ----------------------------------------------------------------------------------------------------------------------


Session = sessionmaker(bind=engine)
session = Session()
