from sqlalchemy import desc, func
from itertools import groupby


class ToggleNoteManager(object):
    @classmethod
    def project_stats(cls, session, to_json=False):
        from models import ToggleNote, ToggleProject

        stats = session.query(
            ToggleProject.name,
            func.sum(ToggleNote.duration / 60).label('time_sum')
        ).filter(ToggleNote.project_id == ToggleProject.id) \
            .group_by(ToggleProject.name) \
            .order_by(desc('time_sum')) \
            .all()
        return stats

    @classmethod
    def client_stats(cls, session, to_json=False):
        from models import ToggleNote, ToggleClient, ToggleProject

        stats = session.query(
            ToggleClient.name,
            func.sum(ToggleNote.duration / 60).label('time_sum')
        ).join(ToggleProject) \
            .join(ToggleNote) \
            .group_by(ToggleClient.name) \
            .order_by(desc('time_sum')) \
            .all()

        return stats

    @classmethod
    def tags_stats(cls, session, to_json=False):
        from models import tag_note_linkage, ToggleNote, ToggleTags

        stats = session.query(
            ToggleTags.name,
            func.sum(ToggleNote.duration / 60).label('time_sum')
        ).join(tag_note_linkage) \
            .join(ToggleNote) \
            .group_by(ToggleTags.name) \
            .order_by(desc('time_sum')) \
            .all()

        return stats

    @classmethod
    def start_x_duration(cls, session, to_json=False):
        from models import ToggleNote, ToggleProject

        stats = session.query(ToggleNote.start, ToggleNote.duration, ToggleProject.color, ToggleProject.name).order_by(
            ToggleProject.name).join(ToggleProject).all()
        if not to_json:
            return stats
        START = 0
        DURATION = 1
        COLOR = 2
        PROJECT_NAME = 3

        data = {'grouped_res': []}
        for name, group in groupby(stats, lambda key: key[PROJECT_NAME]):
            group_obj = {}
            for item in group:
                processed_stats = [item[START], item[DURATION] / 60]

                group_obj['name'] = item[PROJECT_NAME]
                group_obj['color'] = item[COLOR]

                group_obj.setdefault('data', []).append(processed_stats)
            data['grouped_res'].append(group_obj)

        return data

    @classmethod
    def start_x_bubble(cls, session, to_json=False):
        from models import ToggleNote, ToggleProject

        stats = session.query(ToggleNote.start, ToggleNote.duration, ToggleNote.description, ToggleProject.color,
                              ToggleProject.name).order_by(
            ToggleProject.name).join(ToggleProject).all()

        START = 0
        DURATION = 1
        DESCROPTION = 2
        COLOR = 3
        PROJECT_NAME = 4

        grouped_res = []
        for name, group in groupby(stats, lambda key: key[PROJECT_NAME]):
            group_obj = {}
            for item in group:
                processed_stats = {
                    'x': item[START],
                    'y': item[START],
                    'z': item[DURATION] / 60,
                    'desc': item[DESCROPTION]
                }

                group_obj['name'] = item[PROJECT_NAME]
                group_obj['color'] = item[COLOR]

                group_obj.setdefault('data', []).append(processed_stats)
            grouped_res.append(group_obj)

        return grouped_res
