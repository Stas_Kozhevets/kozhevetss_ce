from SimpleXMLRPCServer import SimpleXMLRPCServer
import inspect
from collections import Iterable, Callable
from functools import partial
from itertools import chain


def important(arg):
    """Generic decorator for dealing with priority of procedures. Some funny implementation
    of CSS-like rules. Make sure that your decorators are applied before @classmethod or @staticmethod."""
    if isinstance(arg, int):
        def decorator(func):
            func.priority = arg
            return func

        return decorator
    if isinstance(arg, Callable):
        arg.priority = 1
        return arg
    else:
        raise TypeError('int required')


class RPCServer(object):
    """Allows to collect procedures from classes and functions and call them
    in respect to their importance"""
    def __init__(self, address, classes=None, functions=None):
        self._data = []
        self._serv = SimpleXMLRPCServer(address, allow_none=True)
        self._initialize(classes or (), functions)

    def _collect_callables(self, cls_instances, functions):
        temp = {}
        for f, n, p in chain(self._cls_methods_unpack(cls_instances), self._func_methods_unpack(functions)):
            if (n in temp and p >= temp[n]['priority']) or (n not in temp):
                temp[n] = {'function': f, 'priority': p}

        return temp

    def _register_all(self, *args):
        d = self._collect_callables(*args)
        for entry in d:
            self.register_function(d[entry]['function'], name=entry)
            self._data.append(entry)

    def _cls_methods_unpack(self, cls_instances):
        for cls_instance in cls_instances:
            if cls_instance is not None and inspect.isclass(cls_instance):
                method_instance = cls_instance()
                methods = inspect.getmembers(cls_instance, predicate=inspect.ismethod)
                functions = inspect.getmembers(cls_instance, predicate=inspect.isfunction)
                for name, method in methods:
                    priority = getattr(method, 'priority', 0)
                    unnamed_method = partial(getattr(cls_instance, name), method_instance)

                    yield (unnamed_method, method.__name__, priority)
                for name, func in functions:
                    priority = getattr(method, 'priority', 0)
                    yield (func, func.__name__, priority)

    def _func_methods_unpack(self, functions):
        if not isinstance(functions, Iterable):
            return
        for func in functions:
            name = None
            if isinstance(func, Iterable):
                name, func = func
            if not isinstance(func, Callable):
                raise TypeError('%r must be callable' % func)
            if not name:
                name = func.__name__
            priority = getattr(func, 'priority', 0)
            yield (func, name, priority)

    def _initialize(self, classes, functions):
        self._register_all(classes, functions)
        self.register_function(getattr(self, 'list_funcs'), 'list_funcs')

    def register_function(self, function, name=None):
        self._serv.register_function(function, name=name)

    def serve_forever(self):
        self._serv.serve_forever()

    def list_funcs(self):
        return ', '.join(self._data)


@important(10)
def r(a, v, b):
    return a ** v ** b


class A(object):
    @important(6)
    def add(self, x, y):
        print 'A'
        return x + y


class B(A):
    @important(5)
    def add(self, x, y):
        return x + y

    def r(self, d):
        return d


if __name__ == '__main__':
    CLASSES = (A, B)
    FUNCS = (r,)
    rpc_server = RPCServer(('', 15001), CLASSES, FUNCS)
    rpc_server.serve_forever()
