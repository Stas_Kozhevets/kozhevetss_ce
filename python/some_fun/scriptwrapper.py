import os
import re
from contextlib import contextmanager

class cd(object):
    def __init__(self, target_path):
        self.target_path = target_path
        self.old_path = os.getcwd()

    def __enter__(self):
        os.chdir(self.target_path)

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.chdir(self.old_path)


class home(cd):
    def __init__(self):
        super(home, self).__init__(os.environ['HOME'])


@contextmanager
def section(filename, section_name):
    """
    Writes given commands to target file. If section already exists - appends new lines to the end
    :param filename: path to the file
    :param section_name: name of section wrapped in "# ... __section_name__"""
    start_name, end_name = '# START ' + section_name, '# END ' + section_name
    end_section = re.compile(r'^' + end_name + '$')
    open_mode, exists = ('r+', True) if os.path.exists(filename) else ('w+', False)
    with open(filename, open_mode) as target:
        new_commands, file_content = [], []
        inject_pos = None
        last_index = 0
        if not exists:
            for instruction in (start_name, end_name):
                file_content.append(instruction)
                target.write(instruction + '\n')
                inject_pos = 1

        else:
            for index, line in enumerate(target):
                file_content.append(line.strip())
                last_index = index

                if re.search(end_section, line):
                    inject_pos = index

    if inject_pos is None:
        file_content.extend([start_name, end_name])
        inject_pos = last_index + 2

    yield new_commands

    new_file = file_content[:inject_pos] + new_commands + file_content[inject_pos:]
    with open(filename, 'r+') as target:
        for line in new_file:
            target.write(line + '\n')


def add_shebang(scr_type):
    s = {
            'sh': '#!/bin/sh',
            'bash': '#!/bin/bash',
            'python': '#!/usr/bin/env python'
        }.get(scr_type, '') + '\n'


if __name__ == '__main__':
    new = ['git clone', 'git fetch']
    filename = "test.txt"
    with section(filename, 'test_section') as sect:
        for command in new:
            sect.append(command)

